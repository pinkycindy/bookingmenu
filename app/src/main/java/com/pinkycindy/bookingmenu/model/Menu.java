package com.pinkycindy.bookingmenu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pinky Cindy
 */

public class Menu implements Parcelable {
    @SerializedName("name")
    private String name;

    @SerializedName("category_name")
    private String categoryName;

    @SerializedName("photo")
    private String photo;

    @SerializedName("price")
    private String price;

    public Menu(String name, String categoryName, String photo, String price) {
        super();
        this.name = name;
        this.categoryName = categoryName;
        this.photo = photo;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.categoryName);
        dest.writeString(this.photo);
        dest.writeString(this.price);
    }

    protected Menu(Parcel in) {
        this.name = in.readString();
        this.categoryName = in.readString();
        this.photo = in.readString();
        this.price = in.readString();
    }

    public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel source) {
            return new Menu(source);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };
}
