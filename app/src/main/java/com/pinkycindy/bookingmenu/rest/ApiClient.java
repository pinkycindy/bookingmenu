package com.pinkycindy.bookingmenu.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pinky Cindy on 12/26/18.
 */

public class ApiClient {

    public static Retrofit retrofit;
    public static final String BASE_URL = "https://demo0321724.mockable.io";

    public static ApiInterface getClient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiInterface api = retrofit.create(ApiInterface.class);
        return api;

    }
}
