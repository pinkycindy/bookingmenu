package com.pinkycindy.bookingmenu.rest;


import com.pinkycindy.bookingmenu.features.menus.MenuResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Pinky Cindy on 12/26/18.
 */

public interface ApiInterface {

    @GET("/get-menus")
    Call<MenuResponse> getJSON();
}
