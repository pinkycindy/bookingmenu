package com.pinkycindy.bookingmenu.features.detailmenus;

import com.pinkycindy.bookingmenu.model.Menu;

/**
 * Created by Pinky Cindy
 */

public interface DetailMenuContact {

    interface View{
        void  showMenu(Menu menu);
    }

    interface Presenter{
        void loadMenu(Menu menu);

    }
}
