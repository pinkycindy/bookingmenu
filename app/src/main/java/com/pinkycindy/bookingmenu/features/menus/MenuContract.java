package com.pinkycindy.bookingmenu.features.menus;

import android.support.annotation.NonNull;

import com.pinkycindy.bookingmenu.model.Menu;

import java.util.ArrayList;

/**
 * Created by Pinky Cindy
 */

public interface MenuContract {

    interface View {
        void showProgress();
        void hideProgress();
        void showDataMenus(ArrayList<Menu> menus);
        void showDetailMenus(Menu menu);
        void showCart();

    }

    interface presenter{
        void loadMenus();
        void loadDetailMenus(@NonNull Menu menu);
        void loadCart();


    }
}
