package com.pinkycindy.bookingmenu.features.menus;

import android.util.Log;

import com.pinkycindy.bookingmenu.model.Menu;
import com.pinkycindy.bookingmenu.rest.ApiClient;
import com.pinkycindy.bookingmenu.rest.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pinky Cindy
 */

public class MenuPresenter implements MenuContract.presenter  {


    private MenuContract.View mView;

    public MenuPresenter(MenuContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadMenus() {
        mView.showProgress();
        ApiInterface request = ApiClient.getClient();
        Call<MenuResponse> call = request.getJSON();
        call.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                Log.d("Berhasil", response.toString());

                MenuResponse menu = response.body();
                ArrayList<Menu> menuList;
                menuList = new ArrayList<>(Arrays.asList(menu.getData()));
                Log.d("data", String.valueOf(menuList.size()));

                if(menuList.isEmpty()) {
                   mView.hideProgress();
                }
                else {
                    mView.showDataMenus(menuList);
                    mView.hideProgress();
                }


            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                Log.d("eror", t.toString());
            }
        });

    }

    @Override
    public void loadDetailMenus(Menu menu) {
        mView.showDetailMenus(menu);
    }

    @Override
    public void loadCart() {
        mView.showCart();
    }
}
