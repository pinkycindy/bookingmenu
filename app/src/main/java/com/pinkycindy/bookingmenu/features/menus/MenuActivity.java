package com.pinkycindy.bookingmenu.features.menus;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pinkycindy.bookingmenu.Base.BaseActivity;
import com.pinkycindy.bookingmenu.Base.RecyclerTouchListener;
import com.pinkycindy.bookingmenu.R;
import com.pinkycindy.bookingmenu.features.cart.CartActivity;
import com.pinkycindy.bookingmenu.features.detailmenus.DetailMenuActivity;
import com.pinkycindy.bookingmenu.model.Menu;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Pinky Cindy
 */

public class MenuActivity extends BaseActivity implements MenuContract.View{

    @BindView(R.id.null_task)
    LinearLayout mNoData;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.iv_cart)
    ImageView ivCart;

    private ArrayList<Menu> menuList;
    private MenuAdapter menuAdapter;
    private MenuContract.presenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        toolbarTitle.setText(toolbar.getTitle());
        ivCart.setImageResource(R.drawable.ic_cart);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        menuAdapter= new MenuAdapter(new ArrayList<Menu>(), getApplicationContext());
        presenter = new MenuPresenter(this);

        progressDialog = new ProgressDialog(MenuActivity.this);
       // progressDialog.setCancelable(false); // set cancelable to false


        initRecycler();



    }

    @OnClick(R.id.iv_cart)
    public void onClick(View view) {
        presenter.loadCart();
    }

    @Override
    public void showCart() {
        Intent i = new Intent(this, CartActivity.class);
        i.putExtra(DATA, "Data");
        startActivity(i);

    }



    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadMenus();
    }

    private void initRecycler() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showProgress() {

        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showDataMenus(ArrayList<Menu> menus) {

        menuAdapter = new MenuAdapter(menus, getApplicationContext());
        recyclerView.setAdapter(menuAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(MenuActivity.this, recyclerView,
                new RecyclerTouchListener.ClickListener(){
                    @Override
                    public void onLongClick(View view, int position) {

                    }

                    @Override
                    public void onClick(View view, int position) {
                        Menu menus = menuAdapter.getItem(position);
                        presenter.loadDetailMenus(menus);
                    }
                }
        ));
    }

    @Override
    public void showDetailMenus(Menu menu) {
        Intent i = new Intent(this, DetailMenuActivity.class);
        i.putExtra(DATA, menu);
        startActivity(i);
    }
}
