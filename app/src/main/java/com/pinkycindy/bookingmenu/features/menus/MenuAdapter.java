package com.pinkycindy.bookingmenu.features.menus;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pinkycindy.bookingmenu.R;
import com.pinkycindy.bookingmenu.model.Menu;

import java.util.ArrayList;

/**
 * Created by Pinky Cindy
 */

public class MenuAdapter  extends RecyclerView.Adapter<MenuAdapter.MenusViewHolder>{

    private ArrayList<Menu> data;
    Context context;

    public MenuAdapter(ArrayList<Menu> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public MenuAdapter.MenusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu,parent,false);
        return new MenusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuAdapter.MenusViewHolder holder, int position) {
        holder.name.setText(data.get(position).getName());
        holder.price.setText(data.get(position).getPrice());
        holder.category.setText(data.get(position).getCategoryName());
        Glide.with(context)
                .load(data.get(position).getPhoto())
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Menu getItem(int position) {
            return data.get(position);
    }

    public class MenusViewHolder extends RecyclerView.ViewHolder{
        private TextView name, price;
        private ImageView img;
        private Button category;

        public MenusViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.tTitle);
            price = (TextView)itemView.findViewById(R.id.tPrice);
            img = (ImageView)itemView.findViewById(R.id.img);
            category = (Button)itemView.findViewById(R.id.btnCategory);
        }
    }
}
