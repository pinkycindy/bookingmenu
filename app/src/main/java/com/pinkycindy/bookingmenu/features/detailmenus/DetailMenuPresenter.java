package com.pinkycindy.bookingmenu.features.detailmenus;

import com.pinkycindy.bookingmenu.model.Menu;

/**
 * Created by Pinky Cindy
 */

public class DetailMenuPresenter  implements DetailMenuContact.Presenter{

    private final DetailMenuContact.View mView;

    public DetailMenuPresenter(DetailMenuContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void loadMenu(Menu menu) {
        mView.showMenu(menu);
    }
}
