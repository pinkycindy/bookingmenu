package com.pinkycindy.bookingmenu.features.menus;

import com.pinkycindy.bookingmenu.model.Menu;

/**
 * Created by Pinky Cindy
 */

public class MenuResponse {

    private Menu[] data;

    public Menu[] getData() {
        return data;
    }

}
