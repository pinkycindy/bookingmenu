package com.pinkycindy.bookingmenu.features.detailmenus;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pinkycindy.bookingmenu.Base.BaseActivity;
import com.pinkycindy.bookingmenu.R;
import com.pinkycindy.bookingmenu.model.Menu;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pinky Cindy
 */

public class DetailMenuActivity extends BaseActivity implements DetailMenuContact.View {



    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_price) TextView tvPrice;
    @BindView(R.id.tv_category) TextView tvcategory;
    @BindView(R.id.iv_img) ImageView ivImg;

    private DetailMenuContact.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_detail);
        unbinder = ButterKnife.bind(this);

        presenter = new DetailMenuPresenter(this);
        Menu menu = (Menu) getIntent().getParcelableExtra(DATA);
        presenter.loadMenu(menu);
    }

    @Override
    public void showMenu(Menu menu) {
        tvTitle.setText(menu.getName());
        tvcategory.setText(menu.getCategoryName());
        tvPrice.setText(menu.getPrice());
        Glide.with(DetailMenuActivity.this)
                .load(menu.getPhoto())
                .into(ivImg);


    }

}
